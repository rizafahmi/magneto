//
//  edAppDelegate.h
//  Magneto
//
//  Created by Riza Fahmi on 2/18/14.
//  Copyright (c) 2014 Elixir Dose. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface edAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
