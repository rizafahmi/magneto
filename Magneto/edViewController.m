//
//  edViewController.m
//  Magneto
//
//  Created by Riza Fahmi on 2/18/14.
//  Copyright (c) 2014 Elixir Dose. All rights reserved.
//

#import "edViewController.h"

@interface edViewController ()

@end

@implementation edViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    if([CLLocationManager locationServicesEnabled] && [CLLocationManager headingAvailable]) {
        [locationManager startUpdatingLocation];
        [locationManager startUpdatingHeading];
        locationManager.headingFilter = kCLHeadingFilterNone;
    }else{
        NSLog(@"Can't report heading");
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    if (newHeading.headingAccuracy > 0){
        float magneticHeading = newHeading.magneticHeading;
        float trueHeading = newHeading.trueHeading;
        
        self.magneticLabel.text = [NSString stringWithFormat:@"%f", magneticHeading];
        self.trueLabel.text = [NSString stringWithFormat:@"%f", trueHeading];
        
        float heading = -1.0f * M_PI * newHeading.magneticHeading / 180.0f;
        self.arrowImageView.transform = CGAffineTransformMakeRotation(heading);
    }
}

@end
