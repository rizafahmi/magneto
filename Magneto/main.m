//
//  main.m
//  Magneto
//
//  Created by Riza Fahmi on 2/18/14.
//  Copyright (c) 2014 Elixir Dose. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "edAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([edAppDelegate class]));
    }
}
