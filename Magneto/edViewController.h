//
//  edViewController.h
//  Magneto
//
//  Created by Riza Fahmi on 2/18/14.
//  Copyright (c) 2014 Elixir Dose. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface edViewController : UIViewController <CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *magneticLabel;
@property (weak, nonatomic) IBOutlet UILabel *trueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;

@end
